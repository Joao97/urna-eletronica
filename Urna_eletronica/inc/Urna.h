#ifndef URNA_H
#define URNA_H

#include <vector>
#include "Candidato.h"
#include "Eleitor.h"

using namespace std;

class Urna
{
    public:
        Urna();
        virtual ~Urna();
        void buscar_candidato(int numero_candidato, int tamanho_vetor, vector<Candidato> lista_candidatos);
        void extrair_dados(int tamanho_vetor, vector<Candidato> lista_candidatos);
        void imprimir_candidato(Candidato lista_candidatos);
        int confirmar_voto();
        void votar(vector<Candidato> candidato_BR, vector<Candidato> candidato_DF, Eleitor* eleitor);
        void informar_vencedores(vector<Candidato> lista_df, int qtd_candidatos_df, vector<Candidato *> lista_br, int qtd_candidatos_br);
        void gerar_relatorio(vector<Eleitor*> eleitor, int quantidade_eleitores);
};

#endif
