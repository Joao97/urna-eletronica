#ifndef CANDIDATO_H
#define CANDIDATO_H

#include <Pessoa.h>


class Candidato : public Pessoa
{
    public:
        Candidato();
        virtual ~Candidato();

        string Getunidade_federativa() { return unidade_federativa; }
        void Setunidade_federativa(string val) { unidade_federativa = val; }
        string Getcargo() { return cargo; }
        void Setcargo(string val) { cargo = val; }
        int Getnumero_candidato() { return numero_candidato; }
        void Setnumero_candidato(int val) { numero_candidato = val; }
        string Getnome() { return nome; }
        void Setnome(string val) { nome = val; }
        string Getsigla_partido() { return sigla_partido; }
        void Setsigla_partido(string val) { sigla_partido = val; }
        int Getidade_na_posse() { return idade_na_posse; }
        void Setidade_na_posse(int val) { idade_na_posse = val; }
        string Getgenero() { return genero; }
        void Setgenero(string val) { genero = val; }
        string Getgrau_instrucao() { return grau_instrucao; }
        void Setgrau_instrucao(string val) { grau_instrucao = val; }
        string Getnome_partido() { return nome_partido; }
        void Setnome_partido(string val) { nome_partido = val; }
        int Getquantidade_votos() { return quantidade_votos; }
        void Setquantidade_votos() { quantidade_votos++; }


    private:
        string unidade_federativa;
        string cargo;
        int numero_candidato;
        string nome;
        string sigla_partido;
        int idade_na_posse;
        string genero;
        string grau_instrucao;
        string nome_partido;
        int quantidade_votos;
};

#endif
