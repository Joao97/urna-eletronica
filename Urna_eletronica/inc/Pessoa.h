#ifndef PESSOA_H
#define PESSOA_H
#include <string>

using namespace std;

class Pessoa{
    private:
        string nome;

    public:
        Pessoa();
        virtual ~Pessoa();

        string Getnome() { return nome; }
        void Setnome(string val) { nome = val; }
};

#endif
