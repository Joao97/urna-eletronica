#include "Urna.h"
#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <ctype.h>
#include <vector>
#include <stdio.h>

#define QUANTIDADE_DE_CANDIDATOS_BR 27
#define QUANTIDADE_DE_CANDIDATOS_DF 1201

using namespace std;

Urna::Urna() {}

Urna::~Urna() {}


void imprimir_candidato(Candidato lista_candidatos) {
    cout << lista_candidatos.Getnome() << endl;
    cout << lista_candidatos.Getnumero_candidato() << endl;
    cout << lista_candidatos.Getsigla_partido() << " - " << lista_candidatos.Getnome_partido() << endl;
    cout << lista_candidatos.Getcargo() << endl;
    cout << "genero: " << lista_candidatos.Getgenero() << endl;
    cout << "idade na posse: "<< lista_candidatos.Getidade_na_posse() << " anos" <<endl;
    cout << lista_candidatos.Getgrau_instrucao() << endl;
}

int buscar_candidato(int numero_candidato, int tamanho_vetor, vector<Candidato *> lista_candidatos) {
    int tamanho_chapa=0, aux_chapa;
    int chapa[3];

    for(int icandidato=0; icandidato<tamanho_vetor; icandidato++) {
        if( numero_candidato == lista_candidatos[icandidato]->Getnumero_candidato()) {
            chapa[tamanho_chapa] = icandidato;
            tamanho_chapa++;
        }
    }
    if(tamanho_chapa == 3) {
        if(lista_candidatos[chapa[0]]->Getcargo().compare("1o SUPLENTE") == 0) {
            aux_chapa = chapa[0];
            chapa[0] = chapa[1];
            chapa[1] = aux_chapa;
            if(lista_candidatos[chapa[0]]->Getcargo().compare("2o SUPLENTE") == 0) {
                aux_chapa = chapa[0];
                chapa[0] = chapa[2];
                chapa[2] = aux_chapa;
            }
        } else if(lista_candidatos[chapa[0]]->Getcargo().compare("2o SUPLENTE") == 0) {
            aux_chapa = chapa[0];
            chapa[0] = chapa[2];
            chapa[2] = aux_chapa;
            if(lista_candidatos[chapa[0]]->Getcargo().compare("1o SUPLENTE") == 0) {
                aux_chapa = chapa[0];
                chapa[0] = chapa[1];
                chapa[1] = aux_chapa;
            }
        } else {
            if(lista_candidatos[chapa[1]]->Getcargo().compare("2o SUPLENTE") == 0) {
                aux_chapa = chapa[1];
                chapa[1] = chapa[2];
                chapa[2] = aux_chapa;
            }
        }
    } else if(tamanho_chapa == 2) {
        if(lista_candidatos[chapa[0]]->Getunidade_federativa().compare("BRASIL") == 0) {
            if(lista_candidatos[chapa[0]]->Getcargo().compare("VICE-PRESIDENTE") == 0) {
                aux_chapa = chapa[0];
                chapa[0] = chapa[1];
                chapa[1] = aux_chapa;
            }
        } else {
            if(lista_candidatos[chapa[0]]->Getcargo().compare("VICE-GOVERNADOR") == 0) {
                aux_chapa = chapa[0];
                chapa[0] = chapa[1];
                chapa[1] = aux_chapa;
            }
        }
    }
    for(int j = 0; j<tamanho_chapa; j++) {
        imprimir_candidato(*lista_candidatos[chapa[j]]);
    }
    if(tamanho_chapa <=0) {
        return -1;
    } else return chapa[0];
}

void extrair_dados(int tamanho_vetor, vector<Candidato *> lista_candidatos) {

    ifstream candidatos_txt;
    if(tamanho_vetor == QUANTIDADE_DE_CANDIDATOS_BR) {
        candidatos_txt.open("candidatos_BR.txt");
    } else if(tamanho_vetor == QUANTIDADE_DE_CANDIDATOS_DF) {
        candidatos_txt.open("candidatos_DF.txt");
    }

    string coluna;
    if(!candidatos_txt) {
        cout<<"nao abriu o arquivo\n";
        return;
    }
    if (candidatos_txt.good()) {
        int i=0;
        while (getline(candidatos_txt, coluna, ',')) {
            lista_candidatos[i]->Setunidade_federativa(coluna);
            getline(candidatos_txt, coluna, ',');
            lista_candidatos[i]->Setcargo(coluna);
            getline(candidatos_txt, coluna, ',');
            lista_candidatos[i]->Setnumero_candidato(atoi(coluna.c_str()));
            getline(candidatos_txt, coluna, ',');
            lista_candidatos[i]->Setnome(coluna);
            getline(candidatos_txt, coluna, ',');
            lista_candidatos[i]->Setsigla_partido(coluna);
            getline(candidatos_txt, coluna, ',');
            lista_candidatos[i]->Setnome_partido(coluna);
            getline(candidatos_txt, coluna, ',');
            lista_candidatos[i]->Setidade_na_posse(atoi(coluna.c_str()));
            getline(candidatos_txt, coluna, ',');
            lista_candidatos[i]->Setgenero(coluna);
            getline(candidatos_txt, coluna);
            lista_candidatos[i]->Setgrau_instrucao(coluna);
            i++;

            if(i >= tamanho_vetor)
                break;
        }
        candidatos_txt.close();
    } else cout << "Unable to open file";
}

int confirmar_voto() {
    string botao;

    while(1) {
        cout << "\nDigite \"CONFIRMA\" para confirmar." << endl << "Digite \"CORRIGE\" para cancelar.\n" << endl;
        cin >> botao;

        if(botao.compare("CONFIRMA") == 0) {
            return 1;
        }
        if(botao.compare("CORRIGE") == 0) {
            return 0;
        }
    }
    return 0;
}

void votar(vector<Candidato *> candidato_BR, vector<Candidato *> candidato_DF, Eleitor *eleitor) {

    string voto;
    int voto_valido;
    string cargos[4] = {"Deputado Distrital", "Deputado Federal", "Senador", "Governador"};
    int botao_numerico = 0;
    string votos[5];

    for(int i = 0; i<4; i++) {
        system("cls");
        botao_numerico = 0;
        while(botao_numerico == 0) {
            cout << "digite o numero de seu candidato a " << cargos[i] << ":" << endl;
            cin >> voto;
            if(voto.length() != 5 && cargos[i] == "Deputado Distrital") continue;
            if(voto.length() != 4 && cargos[i] == "Deputado Federal") continue;
            if(voto.length() != 3 && cargos[i] == "Senador") continue;
            if(voto.length() != 2 && cargos[i] == "Governador") continue;

            if(isdigit(voto[0])) {
                voto_valido = buscar_candidato(atoi(voto.c_str()), QUANTIDADE_DE_CANDIDATOS_DF, candidato_DF);
                botao_numerico = confirmar_voto();
                if(botao_numerico == 1 && voto_valido != -1) {
                    candidato_DF[voto_valido]->Setquantidade_votos();
                } else if(botao_numerico == 1 && voto_valido == -1) {
                    voto = "NULO";
                }
            } else if(voto.compare("BRANCO") == 0) {
                cout << "Voce esta votando em branco." << endl;
                botao_numerico = confirmar_voto();
            }
        }
        votos[i] = voto;
    }
    botao_numerico = 0;
    system("cls");
    while(botao_numerico == 0) {
        cout << "digite o numero de seu candidato a Presidente:" << endl;
        cin >> voto;
        if(voto.length() != 2) continue;
        if(isdigit(voto[0])) {
            voto_valido = buscar_candidato(atoi(voto.c_str()), QUANTIDADE_DE_CANDIDATOS_BR, candidato_BR);
            botao_numerico = confirmar_voto();
            if(botao_numerico == 1 && voto_valido != -1) {
                candidato_BR[voto_valido]->Setquantidade_votos();
            } else if(botao_numerico == 1 && voto_valido == -1) {
                voto = "NULO";
            }
        } else if(voto.compare("BRANCO") == 0) {
            cout << "Voce esta votando em branco." << endl;
            botao_numerico = confirmar_voto();
        }
    }
    system("cls");

    votos[4] = voto;
    eleitor->Setvoto_distrital(votos[0]);
    eleitor->Setvoto_federal(votos[1]);
    eleitor->Setvoto_senador(votos[2]);
    eleitor->Setvoto_governador(votos[3]);
    eleitor->Setvoto_presidente(votos[4]);
}

vector<Candidato *> instanciar_vetor_candidato(int quantidade_candidatos, vector<Candidato *> candidatos_geral) {
    Candidato* candidato;
    for(int i = 0; i < quantidade_candidatos; i++) {
        candidato = new Candidato();
        candidatos_geral.push_back(candidato);
    }
    return candidatos_geral;
}

vector<Eleitor *> instanciar_vetor_eleitor(int quantidade_eleitores, vector<Eleitor *> eleitores_geral) {
    Eleitor* eleitor;
    for(int i = 0; i < quantidade_eleitores; i++) {
        eleitor = new Eleitor();
        eleitores_geral.push_back(eleitor);
    }
    return eleitores_geral;
}

void informar_vencedores(vector<Candidato *> lista_df, int qtd_candidatos_df, vector<Candidato *> lista_br, int qtd_candidatos_br) {

    int mais_votado[5]= {-1, -1, -1, -1, -1};
    int indice_mais_votado[5]= {-1, -1, -1, -1, -1};

    for(int i = 0; i < qtd_candidatos_df; i++) {
        if(lista_df[i]->Getcargo().compare("DEPUTADO DISTRITAL") == 0) {
            if(lista_df[i]->Getquantidade_votos() >= mais_votado[0]) {
                mais_votado[0] = lista_df[i]->Getquantidade_votos();
                indice_mais_votado[0] = i;
            }
        }
        if(lista_df[i]->Getcargo().compare("DEPUTADO FEDERAL") == 0) {
            if(lista_df[i]->Getquantidade_votos() >= mais_votado[1]) {
                mais_votado[1] = lista_df[i]->Getquantidade_votos();
                indice_mais_votado[1] = i;
            }
        }
        if(lista_df[i]->Getcargo().compare("SENADOR") == 0) {
            if(lista_df[i]->Getquantidade_votos() >= mais_votado[2]) {
                mais_votado[2] = lista_df[i]->Getquantidade_votos();
                indice_mais_votado[2] = i;
            }
        }
        if(lista_df[i]->Getcargo().compare("GOVERNADOR") == 0) {
            if(lista_df[i]->Getquantidade_votos() >= mais_votado[3]) {
                mais_votado[3] = lista_df[i]->Getquantidade_votos();
                indice_mais_votado[3] = i;
            }
        }
    }
    for(int i = 0; i < qtd_candidatos_br; i++) {
        if(lista_br[i]->Getcargo().compare("PRESIDENTE") == 0) {
            if(lista_br[i]->Getquantidade_votos() >= mais_votado[4]) {
                mais_votado[4] = lista_br[i]->Getquantidade_votos();
                indice_mais_votado[4] = i;
            }
        }
    }

    for(int i = 0; i < 4; i++) {
        cout << "\nO " << lista_df[indice_mais_votado[i]]->Getcargo() << " vencedor foi: \n" << endl;
        if(mais_votado[i] <= 0){
            cout << "\nSem votos para candidato a " << lista_df[indice_mais_votado[i]]->Getcargo() << endl;
        }else{
            imprimir_candidato(*lista_df[indice_mais_votado[i]]);
        }
        cout << "\n-----     -----" << endl;
    }
    cout << "\nO PRESIDENTE vencedor foi:\n" << endl;
    if(mais_votado[4] <= 0){
            cout << "\nSem votos para candidato a PRESIDENTE" << endl;
        }else{
            imprimir_candidato(*lista_br[indice_mais_votado[4]]);
        }
}

void gerar_relatorio(vector<Eleitor*> eleitor, int quantidade_eleitores){
    string temp;
     FILE * csv_eleitores;

     csv_eleitores = fopen("votos_eleitores.csv", "w");
     fprintf(csv_eleitores, "NM_ELEITOR;VOTO_DIS;VOTO_FED;VOTO_SEN;VOTO_GOV;VOTO_PRE");

    for(int i = 0; i<quantidade_eleitores; i++){
     fprintf(csv_eleitores, "\n%s;", eleitor[i]->Getnome().c_str());
     fprintf(csv_eleitores, "%s;", eleitor[i]->Getvoto_distrital().c_str());
     fprintf(csv_eleitores, "%s;", eleitor[i]->Getvoto_federal().c_str());
     fprintf(csv_eleitores, "%s;", eleitor[i]->Getvoto_senador().c_str());
     fprintf(csv_eleitores, "%s;", eleitor[i]->Getvoto_governador().c_str());
     fprintf(csv_eleitores, "%s", eleitor[i]->Getvoto_presidente().c_str());
    }
    cout << "\nO relatorio com os votos dos eleitores foi gerado no arquivo 'votos_eleitores.csv'." << endl;
    fclose(csv_eleitores);
}

int main(int argc, char const *argv[]) {

    vector<Candidato*> candidatos_BR;
    vector<Candidato*> candidatos_DF;
    candidatos_DF = instanciar_vetor_candidato(QUANTIDADE_DE_CANDIDATOS_DF, candidatos_DF);
    candidatos_BR = instanciar_vetor_candidato(QUANTIDADE_DE_CANDIDATOS_BR, candidatos_BR);

    extrair_dados(QUANTIDADE_DE_CANDIDATOS_BR, candidatos_BR);
    extrair_dados(QUANTIDADE_DE_CANDIDATOS_DF, candidatos_DF);

    int quantidade_eleitores;
    cout << "Senhor(a) Mesario(a), por favor informe a quantidade de eleitores nessa secao" << endl;
    cin >> quantidade_eleitores;
    vector<Eleitor*> secao_eleitoral;
    string aux_nome="0";
    secao_eleitoral = instanciar_vetor_eleitor(quantidade_eleitores, secao_eleitoral);
    for(int i=0; i<quantidade_eleitores; i++) {

        getline(cin, aux_nome);
        while(isdigit(aux_nome[0]) || aux_nome.length() < 3){
            cout << "Caro eleitor, por favor insira seu nome:" << endl;
            getline(cin, aux_nome);
        }
        secao_eleitoral[i]->Setnome(aux_nome);
        votar(candidatos_BR, candidatos_DF, secao_eleitoral[i]);
    }

    informar_vencedores(candidatos_DF, QUANTIDADE_DE_CANDIDATOS_DF, candidatos_BR, QUANTIDADE_DE_CANDIDATOS_BR);
    gerar_relatorio(secao_eleitoral, quantidade_eleitores);

    return 0;
}
