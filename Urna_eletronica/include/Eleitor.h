#ifndef ELEITOR_H
#define ELEITOR_H

#include <Pessoa.h>
#include <string>


class Eleitor : public Pessoa
{
    public:
        Eleitor();
        virtual ~Eleitor();

        string Getvoto_distrital() { return voto_distrital; }
        void Setvoto_distrital(string val) { voto_distrital = val; }
        string Getvoto_federal() { return voto_federal; }
        void Setvoto_federal(string val) { voto_federal = val; }
        string Getvoto_senador() { return voto_senador; }
        void Setvoto_senador(string val) { voto_senador = val; }
        string Getvoto_governador() { return voto_governador; }
        void Setvoto_governador(string val) { voto_governador = val; }
        string Getvoto_presidente() { return voto_presidente; }
        void Setvoto_presidente(string val) { voto_presidente = val; }

    private:
        string voto_distrital;
        string voto_federal;
        string voto_senador;
        string voto_governador;
        string voto_presidente;
};

#endif
